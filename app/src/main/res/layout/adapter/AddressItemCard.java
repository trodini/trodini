package layout.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skimp.app.R;

import it.gmariotti.cardslib.library.internal.Card;


public class AddressItemCard extends Card {

    //content variables
    String name, state, id;


    TextView textViewName;
    TextView textViewState;
    TextView textViewId;

    public AddressItemCard(Context context) {
        super(context);
    }

    public AddressItemCard(Context context, String name, String id, String state) {
        super(context, R.layout.layout_address_item);
        this.name = name;
        this.state = state;
        this.id = id;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        textViewName = (TextView) parent.findViewById(R.id.textViewAddressName);
        textViewState = (TextView) parent.findViewById(R.id.textViewIndicator);
        textViewId = (TextView) parent.findViewById(R.id.textViewAddressId);

        textViewName.setText(this.name);
        textViewId.setText(this.id);
        if (this.state.equalsIgnoreCase("shipping")) {
            textViewState.setBackgroundColor(Color.BLUE);
        }else {
            textViewState.setBackgroundColor(Color.RED);
        }
    }
}
