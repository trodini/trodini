package layout.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skimp.app.R;
import com.squareup.picasso.Picasso;

import java.net.URI;

import it.gmariotti.cardslib.library.internal.Card;


public class HistoryItemCard extends Card {

    //content variables
    String name, price, shop, id;
    URI imageURL;

    //UI element variables
    ImageView imageViewItem;
    TextView textViewName;
    TextView textViewPrice;
    TextView textViewShopName;
    TextView textViewId;

    public HistoryItemCard(Context context) {
        super(context);
    }

    public HistoryItemCard(Context context, String name, URI imageURL, String id, String shop, String price) {
        super(context, R.layout.layout_history_item);
        this.name = name;
        this.price = price;
        this.shop = shop;
        this.imageURL = imageURL;
        this.id = id;
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        imageViewItem = (ImageView) parent.findViewById(R.id.imageViewItemImage);
        textViewName = (TextView) parent.findViewById(R.id.textViewItemName);
        textViewPrice = (TextView) parent.findViewById(R.id.textViewItemPrice);
        textViewShopName = (TextView) parent.findViewById(R.id.textViewItemShop);
        textViewId = (TextView) parent.findViewById(R.id.textViewItemId);

        Picasso.with(getContext()).load(this.imageURL.toString()).centerCrop().error(R.drawable.default_bg).placeholder(R.drawable.default_bg)
                .resize(100, 100)
                .into(imageViewItem);


        textViewName.setText(this.name);
        textViewPrice.setText(this.price);
        textViewShopName.setText(this.shop);
        textViewId.setText(this.id);
    }
}
