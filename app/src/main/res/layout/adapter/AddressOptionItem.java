package layout.adapter;

/**
 * Created by bright on 6/9/14.
 */
public class AddressOptionItem {

    String ItemName;
    int imgResID;

    public AddressOptionItem(String itemName) {
        super();
        ItemName = itemName;

    }

    public String getItemName() {
        return ItemName;
    }

    public void setItemName(String itemName) {
        ItemName = itemName;
    }


}
