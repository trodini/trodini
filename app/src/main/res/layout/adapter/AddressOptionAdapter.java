package layout.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.skimp.app.R;

import java.util.List;

public class AddressOptionAdapter extends ArrayAdapter<AddressOptionItem> {

    Context context;
    private ViewHolder holder;


    public AddressOptionAdapter(Context context, int resourceId,
                                List<AddressOptionItem> items) {
        super(context, resourceId, items);
        this.context = context;

    }


    public View getView(int position, View convertView, ViewGroup parent) {
        holder = null;
        final AddressOptionItem rowItem = getItem(position);
        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_address_option_item, null);
            holder = new ViewHolder();
            holder.textView = (TextView) convertView.findViewById(R.id.textView);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.textView.setText(rowItem.getItemName());


        return convertView;
    }


    private class ViewHolder {
        ImageView imageView;
        TextView textView;
    }
}