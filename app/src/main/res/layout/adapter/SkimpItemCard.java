package layout.adapter;

import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.skimp.app.R;
import com.skimp.app.persistence.Product;
import com.squareup.picasso.Picasso;

import java.net.URI;

import it.gmariotti.cardslib.library.internal.Card;


public class SkimpItemCard extends Card {

    //content variables
    String name, price, shop, id;
    URI imageURL;


    //UI element variables
    ImageView imageViewItem;
    TextView textViewName;
    TextView textViewPrice;
    TextView textViewShopName;
    TextView textViewId;
    TextView textViewItemImage;
    TextView buttonAddToCart;

    public SkimpItemCard(Context context) {
        super(context);
    }

    public SkimpItemCard(Context context, String name,
                         URI imageURL, String id, String shop, String price) {
        super(context, R.layout.layout_skimp_item);
        this.name = name;
        this.price = price;
        this.shop = shop;
        this.imageURL = imageURL;
        this.id = id;
    }

    @Override
    public void setupInnerViewElements(final ViewGroup parent, final View view) {
        super.setupInnerViewElements(parent, view);

        imageViewItem = (ImageView) parent.findViewById(R.id.imageViewItemImage);
        textViewName = (TextView) parent.findViewById(R.id.textViewItemName);
        textViewPrice = (TextView) parent.findViewById(R.id.textViewItemPrice);
        textViewShopName = (TextView) parent.findViewById(R.id.textViewItemShop);
        textViewId = (TextView) parent.findViewById(R.id.textViewItemId);
        textViewItemImage = (TextView) parent.findViewById(R.id.textViewItemImage);
        buttonAddToCart = (TextView) parent.findViewById(R.id.buttonAddToCart);

        String image;
        if (this.imageURL.toString().contains(",")) {
            image = this.imageURL.toString().split(",")[0].toString();
        } else {
            image = this.imageURL.toString();
        }

        Picasso.with(getContext())
                .load(image)
                .centerCrop()
                .error(R.color.white_color)
                .resize(200, 200)
                .into(imageViewItem);

        textViewName.setText(this.name);
        textViewPrice.setText(this.price);
        textViewShopName.setText(this.shop);
        textViewId.setText(this.id);
        textViewItemImage.setText(this.imageURL.toString());

        buttonAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String search =  textViewId.getText().toString();
                Log.e("Result",search);
                Product product = Product.find(Product.class, "product_Id = ?",search).get(0);
                product.isCartItem = true;
                product.save();
                parent.setVisibility(View.GONE);
            }
        });


    }
}
