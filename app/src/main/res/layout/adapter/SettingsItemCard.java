package layout.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.skimp.app.R;

import it.gmariotti.cardslib.library.internal.Card;


public class SettingsItemCard extends Card {

    //content variables
    String title, itemOne, itemTwo;

    //UI element variables
    TextView textViewTitle;
    TextView textViewItemOne;
    TextView textViewTwo;


    public SettingsItemCard(Context context) {
        super(context);
    }

    public SettingsItemCard(Context context, String name, String itemOne, String itemTwo) {
        super(context, R.layout.layout_settings_item);
        this.title = name;
        this.itemOne = itemOne;
        this.itemTwo = itemTwo;

    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);


        textViewTitle = (TextView) parent.findViewById(R.id.textViewItemTitle);
        textViewItemOne = (TextView) parent.findViewById(R.id.textViewItemOne);
        textViewTwo = (TextView) parent.findViewById(R.id.textViewItemTwo);


        textViewTitle.setText(this.title);
        textViewItemOne.setText(this.itemOne);
        textViewTwo.setText(this.itemTwo);

    }
}
