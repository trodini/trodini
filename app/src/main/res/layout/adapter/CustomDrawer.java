package layout.adapter;

/**
 * Created by bright on 6/9/14.
 */

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.skimp.app.R;
import com.skimp.app.persistence.App;
import com.skimp.app.persistence.User;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomDrawer extends ArrayAdapter<DrawerItem> {

    private Context context;
    private List<DrawerItem> drawerItemList;
    private int layoutResID;
    private String defaultImage = "http://static.businessinsider.com/image/53d26b63eab8eac12759b0aa-400/19-liz-levy-associate-creative-director-at-tbwachiatday-la.jpg";


    public CustomDrawer(Context context, int layoutResourceID,
                        List<DrawerItem> listItems) {
        super(context, layoutResourceID, listItems);
        this.context = context;
        this.drawerItemList = listItems;
        this.layoutResID = layoutResourceID;


    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        DrawerItemHolder drawerHolder;
        View view = convertView;

        if (view == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            drawerHolder = new DrawerItemHolder();
            view = inflater.inflate(layoutResID, parent, false);
            drawerHolder.ItemName = (TextView) view.findViewById(R.id.drawer_itemName);
            drawerHolder.drawerUserName = (TextView) view.findViewById(R.id.drawerUserName);
            drawerHolder.icon = (ImageView) view.findViewById(R.id.drawer_icon);
            drawerHolder.imageViewProfilePic = (ImageView) view.findViewById(R.id.imageViewProfilePic);
            drawerHolder.linearLayoutAccountSection = (LinearLayout) view.findViewById(R.id.linearLayoutAccountSection);
            drawerHolder.itemLayout = (LinearLayout) view.findViewById(R.id.itemLayout);
            view.setTag(drawerHolder);

        } else {
            drawerHolder = (DrawerItemHolder) view.getTag();

        }
        //check and set the visibilities
        if (position == 0) {
            drawerHolder.itemLayout.setVisibility(View.GONE);
            List<User> user = App.listAll(User.class);
            if (!user.isEmpty()) {
                User currentUser = user.get(0);
                String first_name = currentUser.first_name.toString();
                String last_name = currentUser.last_name.toString();
                drawerHolder.drawerUserName.setText(last_name + " " + first_name);
                if (currentUser.image != null) {

                }
                Picasso.with(getContext()).load(defaultImage)
                        .centerCrop()
                        .error(R.color.white_color)
                        .placeholder(R.color.white_color)
                        .resize(100, 100)
                        .into(drawerHolder.imageViewProfilePic);
            }
        } else {
            drawerHolder.linearLayoutAccountSection.setVisibility(View.GONE);
        }

        DrawerItem dItem = this.drawerItemList.get(position);
        drawerHolder.icon.setImageDrawable(view.getResources().getDrawable(
                dItem.getImgResID()));
        drawerHolder.ItemName.setText(dItem.getItemName());


        return view;
    }

    //instances if the views
    private static class DrawerItemHolder {
        TextView ItemName, drawerUserName;
        ImageView icon, imageViewProfilePic;
        LinearLayout linearLayoutAccountSection, itemLayout;

    }

}

