package layout.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.skimp.app.R;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.view.CardView;

public class CartCardAdapter extends BaseAdapter {

    private ArrayList<CartItemCard> feedItemCardArrayList = new ArrayList<CartItemCard>();
    private Context context;

    public CartCardAdapter(Context context, ArrayList<CartItemCard> feedItemCardArrayList) {
        this.context = context;
        this.feedItemCardArrayList = feedItemCardArrayList;
    }

    @Override
    public int getCount() {
        return feedItemCardArrayList.size();
    }

    @Override
    public CartItemCard getItem(int i) {
        return feedItemCardArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.single_card_view, viewGroup, false);

        CardView cardView = (CardView) v.findViewById(R.id.cardSingleCard);
        cardView.setCard(getItem(i));

        return v;
    }
}
