package com.trodini.app.adapter;


public class HandleItem {

    private String id, startTime, startDate, endTime, endDate, duration, amount, startLat, startLon, endLat, endLon,startLocName,endLocName;
    private boolean isSync;

    public HandleItem(String id, String duration, String amount,String startLocName,String endLocName) {
        this.id = id;
        this.amount = amount;
        this.startLocName = startLocName;
        this.endLocName = endLocName;
        this.duration = duration;

    }

    public String getEndLocName() {
        return endLocName;
    }

    public String getStartLocName() {
        return startLocName;
    }

    public String getId() {
        return id;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public String getAmount() {
        return amount;
    }

    public String getDuration() {
        return duration;
    }

    public String getStartLat() {
        return startLat;
    }

    public String getEndLat() {
        return endLat;
    }

    public String getEndLon() {
        return endLon;
    }

    public String getStartLon() {
        return startLon;
    }

}
