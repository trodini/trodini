package com.trodini.app.adapter;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trodini.app.R;
import com.trodini.app.util.LetterImageView;

import java.util.List;

public class RouteAdapter extends ArrayAdapter<RouteItem> {

    Context context;

    public RouteAdapter(Context context, int resourceId,
                        List<RouteItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        RouteItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_route_item, null);
            holder = new ViewHolder();
            holder.textViewRoute = (TextView) convertView.findViewById(R.id.textViewRoute);
            holder.textViewId = (TextView) convertView.findViewById(R.id.textViewId);
            holder.textViewPriceTime = (TextView) convertView.findViewById(R.id.textViewPriceTime);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Log.e("Item info", rowItem.getId());
        holder.textViewId.setText(rowItem.getId());
        holder.textViewPriceTime.setText(rowItem.getDuration().concat(" - ").concat(rowItem.getPrice()));
        holder.textViewRoute.setText(rowItem.getNameStart().concat(" , ").concat(rowItem.getNameEnd()));


        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        TextView textViewRoute, textViewId, textViewPriceTime;

    }
}