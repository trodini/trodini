package com.trodini.app.adapter;


public class RouteItem {

    private String id,nameStart,nameEnd,duration,price;

    public RouteItem(String id,String nameStart,String nameEnd,String duration,String price) {
        this.id = id;
        this.nameStart = nameStart;
        this.nameEnd = nameEnd;
        this.duration = duration;
        this.price = price;

    }


    public String getNameEnd() {
        return nameEnd;
    }

    public String getNameStart() {
        return nameStart;
    }

    public String getId() {
        return id;
    }

    public String getDuration() {
        return duration;
    }

    public String getPrice() {
        return price;
    }
}
