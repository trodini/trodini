package com.trodini.app.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trodini.app.R;
import com.trodini.app.util.LetterImageView;

import java.util.List;

public class HandleAdapter extends ArrayAdapter<HandleItem> {

    Context context;

    public HandleAdapter(Context context, int resourceId,
                         List<HandleItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        HandleItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_handle_item, null);
            holder = new ViewHolder();
            holder.letterImageView = (LetterImageView) convertView.findViewById(R.id.letterImageView);
            holder.textViewPriceTime = (TextView) convertView.findViewById(R.id.textViewPriceTime);
            holder.textViewId = (TextView) convertView.findViewById(R.id.textViewId);
            holder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textViewId.setText(rowItem.getId());
        holder.textViewName.setText(rowItem.getStartLocName().concat(" - ").concat(rowItem.getEndLocName()));
        holder.textViewPriceTime.setText(rowItem.getDuration().concat(" , ").concat(rowItem.getAmount()));
        holder.letterImageView.setOval(true);
        holder.letterImageView.setLetter(rowItem.getStartLocName().toUpperCase().charAt(0));


        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        LetterImageView letterImageView;
        TextView textViewPriceTime, textViewId, textViewName;

    }
}