package com.trodini.app.adapter;


import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.trodini.app.R;
import com.trodini.app.util.LetterImageView;

import java.util.List;

public class FlagAdapter extends ArrayAdapter<FlagItem> {

    Context context;

    public FlagAdapter(Context context, int resourceId,
                       List<FlagItem> items) {
        super(context, resourceId, items);
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        FlagItem rowItem = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.layout_flag_item, null);
            holder = new ViewHolder();
            holder.letterImageView = (LetterImageView) convertView.findViewById(R.id.letterImageView);
            holder.textViewId = (TextView) convertView.findViewById(R.id.textViewId);
            holder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.textViewId.setText(rowItem.getId());
        holder.textViewName.setText(rowItem.getName());
        holder.letterImageView.setOval(true);
        holder.letterImageView.setLetter(rowItem.getName().toUpperCase().charAt(0));


        return convertView;
    }

    /*private view holder class*/
    private class ViewHolder {
        LetterImageView letterImageView;
        TextView textViewId, textViewName;

    }
}