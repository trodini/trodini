package com.trodini.app.core;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.trodini.app.R;
import com.trodini.app.adapter.CustomDrawer;
import com.trodini.app.adapter.DrawerItem;
import com.trodini.app.fragment.FragmentAccount;
import com.trodini.app.fragment.FragmentFlag;
import com.trodini.app.fragment.FragmentHandle;
import com.trodini.app.fragment.FragmentHome;
import com.trodini.app.fragment.FragmentPlace;
import com.trodini.app.fragment.FragmentSetting;
import com.trodini.app.persistence.App;
import com.trodini.app.util.Misc;

import java.util.ArrayList;
import java.util.List;


public class Main extends ActionBarActivity {
    private CustomDrawer adapter;
    private List<DrawerItem> dataList;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    public ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mTitle;
    private ShareActionProvider mShareActionProvider;
    private ActionBar actionBar;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        context = this;
        List<App> app = App.listAll(App.class);
        if (app.isEmpty()) {
            App newApp = new App(true);
            newApp.save();
        }


        // Initializing
        dataList = new ArrayList<DrawerItem>();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        // Add Drawer Item to dataList
        dataList.add(new DrawerItem(getString(R.string.account), R.drawable.ic_face_grey));
        dataList.add(new DrawerItem(getString(R.string.home), R.drawable.ic_face_grey));
        dataList.add(new DrawerItem(getString(R.string.find), R.drawable.ic_find_replace_grey));
        dataList.add(new DrawerItem(getString(R.string.handles), R.drawable.ic_bookmark_outline_grey));
        dataList.add(new DrawerItem(getString(R.string.flags), R.drawable.ic_apps_grey));
        dataList.add(new DrawerItem(getString(R.string.settings), R.drawable.ic_group_work_grey));

        adapter = new CustomDrawer(this, R.layout.custom_drawer_item,
                dataList);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());


        if (savedInstanceState == null) {
            SelectItem(1);
        }
    }

    public void SelectItem(int position) {
        Fragment fragment = null;
        Bundle args = new Bundle();
        switch (position) {
            case 0:
                fragment = new FragmentAccount();
                break;
            case 1:
                fragment = new FragmentHome();
                break;
            case 2:
                fragment = new FragmentPlace();
                break;
            case 3:
                fragment = new FragmentHandle();
                break;
            case 4:
                fragment = new FragmentFlag();
                break;
            case 5:
                fragment = new FragmentSetting();
                break;
            default:
                break;
        }

        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.content_frame,
                            fragment,
                            "Created the fragment").commit();

            mDrawerList.setItemChecked(position, true);
            setTitle(dataList.get(position).getItemName());
            mDrawerLayout.closeDrawer(mDrawerList);
        }


    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getSupportActionBar().setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.menu_item_share:
                return true;
            case R.id.action_main_feedback:
                Misc.sendFeedback(context);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        MenuItem item = menu.findItem(R.id.menu_item_share);
        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        setShareIntent(getDefaultShareIntent());

        return true;
    }


    private void setShareIntent(Intent shareIntent) {
        if (mShareActionProvider != null) {
            mShareActionProvider.setShareIntent(shareIntent);
        }
    }

    private Intent getDefaultShareIntent() {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, R.string.app_name);
        intent.putExtra(Intent.EXTRA_TEXT, "Download " + R.string.app_name + " at \n");
        return intent;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                                long id) {
            SelectItem(position);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }
}

