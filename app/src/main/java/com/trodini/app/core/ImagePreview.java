package com.trodini.app.core;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.trodini.app.R;
import com.trodini.app.persistence.User;
import com.trodini.app.util.ImageDecoder;
import com.trodini.app.util.ZoomableImageView;

import org.apache.http.Header;

import java.io.File;
import java.util.List;

public class ImagePreview extends Activity {
    private String defaultImage = "http://static.businessinsider.com/image/53d26b63eab8eac12759b0aa-400/19-liz-levy-associate-creative-director-at-tbwachiatday-la.jpg";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_preview);

        final ProgressBarCircularIndeterminate progressBarCircularIndeterminate = (ProgressBarCircularIndeterminate) findViewById(R.id.progressBarLoadImage);
        User user;
        List<User> userList = User.listAll(User.class);
        if (!userList.isEmpty()) {
            user = userList.get(0);
            defaultImage = user.image_link;

        }
        String imageUrl = defaultImage;
        final ZoomableImageView imageView = (ZoomableImageView) findViewById(R.id.imageView);
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(imageUrl, new FileAsyncHttpResponseHandler(this) {
            @Override
            public void onSuccess(int statusCode, Header[] headers, File response) {
                progressBarCircularIndeterminate.setVisibility(View.GONE);
                Bitmap bitmap = ImageDecoder.decodeFile(response.getAbsolutePath());
                imageView.setImageBitmap(bitmap);
                response.deleteOnExit();
            }

            @Override
            public void onFailure(Throwable e, File response) {
                progressBarCircularIndeterminate.setVisibility(View.GONE);
                Toast.makeText(ImagePreview.this, "Failed to load image", Toast.LENGTH_LONG).show();
            }

        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
