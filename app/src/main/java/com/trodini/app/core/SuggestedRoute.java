package com.trodini.app.core;

import android.content.Context;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.trodini.app.R;
import com.trodini.app.adapter.HandleAdapter;
import com.trodini.app.adapter.HandleItem;
import com.trodini.app.adapter.RouteAdapter;
import com.trodini.app.adapter.RouteItem;

import java.util.ArrayList;
import java.util.List;

public class SuggestedRoute extends ActionBarActivity {
    private ActionBar actionBar;
    private ListView listView;
    private Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_suggested_route);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        context = this;

        final List<RouteItem> routeItems = new ArrayList<>();
        listView = (ListView) findViewById(R.id.listViewRoutes);
        final RouteAdapter routeAdapter = new RouteAdapter(context, R.layout.layout_route_item, routeItems);
        listView.setAdapter(routeAdapter);
        routeItems.add(new RouteItem("1", "American House", "Lapaz", "20 mins", "Gh S 1.50"));
        routeItems.add(new RouteItem("2", "Madina", "Sogakope", "1hr 50 mins", "Gh S 12.50"));
        routeItems.add(new RouteItem("3", "North Legon", "Lapaz", "20 mins", "Gh S 1.50"));
        routeItems.add(new RouteItem("4", "American House", "Lapaz", "20 mins", "Gh S 1.50"));
        routeAdapter.notifyDataSetChanged();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //show location and direction

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
