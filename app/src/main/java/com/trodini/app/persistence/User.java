package com.trodini.app.persistence;

import com.orm.SugarRecord;

/**
 * Created by bright on 11/19/14.
 */
public class User extends SugarRecord<User> {

    public String first_name;
    public String last_name;
    public String email;
    public String image_link;
    public String id;
    public String facebook_id;
    public String token;

    public User(String id, String first_name, String last_name, String email, String image_link) {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.image_link = image_link;
    }

    public User() {
    }

}