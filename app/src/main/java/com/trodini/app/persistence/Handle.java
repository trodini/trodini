package com.trodini.app.persistence;

import com.orm.SugarRecord;

/**
 * Created by bright on 2/6/15.
 */
public class Handle extends SugarRecord<Handle> {

    private String id, startTime, startDate, endTime, endDate, duration, amount, startLat, startLon, endLat, endLon, startLocName, endLocName;
    private boolean isSync;

    public Handle() {

    }

    public Handle(String id, String startTime, String startDate, String endTime, String endDate, String startLat, String startLon, String endLat, String endLon, String duration, String amount, String startLocName, String endLocName) {
        this.id = id;
        this.startTime = startTime;
        this.startDate = startDate;
        this.endTime = endTime;
        this.endDate = endDate;
        this.startLat = startLat;
        this.startLon = startLon;
        this.endLat = endLat;
        this.endLon = endLon;
        this.amount = amount;
        this.duration = duration;
        this.endLocName = endLocName;
        this.startLocName = startLocName;
        this.isSync = false;

    }
}
