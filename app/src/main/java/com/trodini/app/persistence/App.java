package com.trodini.app.persistence;

import com.orm.SugarRecord;

/**
 * Created by bright on 11/19/14.
 */
public class App extends SugarRecord<App> {

    public boolean installed;

    public App(boolean installed) {
        this.installed = installed;
    }

    public App() {
    }

}