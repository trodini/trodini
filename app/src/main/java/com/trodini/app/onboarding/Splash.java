package com.trodini.app.onboarding;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.trodini.app.R;
import com.trodini.app.core.Main;
import com.trodini.app.persistence.User;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


public class Splash extends Activity {
    Timer timer = new Timer();
    private int DELAY = 4000;
    private Intent intent;
    private Context context;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        context = this;
        intent = new Intent();
        intent.setClass(context, SignUpOptionsActivity.class);
        textView = (TextView) findViewById(R.id.textView);
        YoYo.with(Techniques.SlideInDown)
                .delay(400)
                .playOn(textView);


        timer.schedule(new TimerTask() {
            public void run() {
                List<User> app = User.listAll(User.class);
                if (app.isEmpty()) {
                    intent.setClass(context, SignUpOptionsActivity.class);
                } else {
                    intent.setClass(context, Main.class);
                }
                startActivity(intent);
            }
        }, DELAY);
    }

    @Override
    protected void onPause() {
        super.onPause();
        timer.cancel();
        overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
        finish();
    }
}
