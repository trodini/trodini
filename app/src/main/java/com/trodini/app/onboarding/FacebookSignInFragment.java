package com.trodini.app.onboarding;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;
import com.trodini.app.R;
import com.trodini.app.core.Main;
import com.trodini.app.persistence.User;
import com.trodini.app.util.Misc;

import java.util.Arrays;


@SuppressLint("NewApi")
public class FacebookSignInFragment extends Fragment {
    private static final String TAG = "FacebookLogin";
    Context mContext;
    LoginButton mLoginBtn;
    private Button buttonLogin, buttonRegister;
    private String regUserEmail, regUserAuthToken, regUserID, regFirstName, regLastName, regGender;
    private String fbId, first_name, last_name, userEmail, profilePic = "", profilePicSmall = "", gender = "", avatar = "", date_of_birth = "", community = "";


    //session status callback variable
    private Session.StatusCallback callback = new Session.StatusCallback() {
        @Override
        public void call(Session session, SessionState state, Exception exception) {
            onSessionStateChange(session, state, exception);
        }
    };

    //manages ui flow
    private UiLifecycleHelper uiHelper;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_facebook_login, container, false);


        mContext = getActivity();

        mLoginBtn = (LoginButton) view.findViewById(R.id.authButton);
        buttonLogin = (Button) view.findViewById(R.id.buttonLogin);
        buttonRegister = (Button) view.findViewById(R.id.buttonRegister);
        buttonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Misc.showNotice(mContext, "Register with facebook");
            }
        });
        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Misc.showNotice(mContext, "Login with facebook");
            }
        });
        mLoginBtn.setFragment(this);
        mLoginBtn.setReadPermissions(Arrays.asList("public_profile", "email"));


        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        uiHelper = new UiLifecycleHelper(getActivity(), callback);
        uiHelper.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        Session session = Session.getActiveSession();
        if (session != null &&
                (session.isOpened() || session.isClosed())) {
            onSessionStateChange(session, session.getState(), null);
        }

        uiHelper.onResume();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        uiHelper.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
        if (state.isOpened()) {
            Log.i(TAG, "Logged in...");
            mLoginBtn.setText("Logging you in...");
            makeMeRequest(session);
        } else if (state.isClosed()) {
            Log.i(TAG, "Logged out...");
            Log.i(TAG, "Facebook Exception " + exception);
        }
    }

    private void makeMeRequest(final Session session) {
        Request request = Request.newMeRequest(session,
                new Request.GraphUserCallback() {
                    @Override
                    public void onCompleted(GraphUser user, final Response response) {
                        // If the response is successful
                        if (session == Session.getActiveSession()) {
                            if (user != null) {
                                first_name = user.getFirstName();
                                last_name = user.getLastName();
                                fbId = user.getId();
                                userEmail = (String) user.asMap().get("email");
                                profilePic = "http://graph.facebook.com/" + fbId + "/picture?type=large";
                                profilePicSmall = "http://graph.facebook.com/" + fbId + "/picture?type=small";
                                gender = user.asMap().get("gender").toString();
                                avatar = profilePic;
                                Log.e(TAG, fbId);
                                User facebookUser = new User();
                                facebookUser.id = fbId;
                                facebookUser.image_link = profilePic;
                                facebookUser.email = userEmail;
                                facebookUser.last_name = last_name;
                                facebookUser.first_name = first_name;
                                facebookUser.token = "";
                                facebookUser.facebook_id = fbId;
                                facebookUser.save();
                                Intent intent = new Intent(mContext, Main.class);
                                startActivity(intent);


                            }

                        }
                        if (response.getError() != null) {
                            Misc.showNotice(mContext, "Error login in");
                        }
                    }
                }
        );

        request.executeAsync();
    }

}