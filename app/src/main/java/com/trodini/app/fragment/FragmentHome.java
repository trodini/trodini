package com.trodini.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.melnykov.fab.FloatingActionButton;
import com.melnykov.fab.ObservableScrollView;
import com.nineoldandroids.animation.Animator;
import com.trodini.app.R;
import com.trodini.app.persistence.User;
import com.trodini.app.util.Misc;

import java.util.List;


public class FragmentHome extends Fragment {
    private ObservableScrollView scrollView;
    private Intent intent;
    private TextView textViewUserName;
    private LinearLayout linearLayoutStart, linearLayoutEnd, linearLayoutPrice;
    private Button buttonRoutStart, buttonRoutReset, buttonRoutEnd, buttonRoutPrice;
    private EditText editTextStartRoute, editTextEnd, editTextPrice;
    private String startLocationName, endLocationName, price;
    private String start;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        intent = new Intent();
        scrollView = (ObservableScrollView) view.findViewById(R.id.scroll_view);

        textViewUserName = (TextView) view.findViewById(R.id.textViewUserName);
        linearLayoutStart = (LinearLayout) view.findViewById(R.id.linearLayoutStart);
        linearLayoutEnd = (LinearLayout) view.findViewById(R.id.linearLayoutEnd);
        linearLayoutPrice = (LinearLayout) view.findViewById(R.id.linearLayoutPrice);
        buttonRoutStart = (Button) view.findViewById(R.id.buttonRoutStart);
        buttonRoutReset = (Button) view.findViewById(R.id.buttonRoutReset);
        buttonRoutEnd = (Button) view.findViewById(R.id.buttonRoutEnd);
        buttonRoutPrice = (Button) view.findViewById(R.id.buttonRoutPrice);
        editTextStartRoute = (EditText) view.findViewById(R.id.editTextStartRoute);
        editTextEnd = (EditText) view.findViewById(R.id.editTextEnd);
        editTextPrice = (EditText) view.findViewById(R.id.editTextPrice);


        start = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("POSITION", "nothing");
        List<User> userList = User.listAll(User.class);
        User user;
        String first_name = "Trodini";
        if (!userList.isEmpty()) {
            user = userList.get(0);
            first_name = user.first_name;

        }
        textViewUserName.setText(Misc.getGreeting().concat(", ").concat(first_name));

        if (start.equalsIgnoreCase("nothing") || start.equalsIgnoreCase("start")) {
            linearLayoutStart.setVisibility(View.VISIBLE);
            linearLayoutEnd.setVisibility(View.GONE);
            linearLayoutPrice.setVisibility(View.GONE);
        } else if (start.equalsIgnoreCase("end")) {
            linearLayoutStart.setVisibility(View.GONE);
            linearLayoutEnd.setVisibility(View.VISIBLE);
            linearLayoutPrice.setVisibility(View.GONE);
        } else {
            linearLayoutStart.setVisibility(View.GONE);
            linearLayoutEnd.setVisibility(View.GONE);
            linearLayoutPrice.setVisibility(View.VISIBLE);
        }


        YoYo.with(Techniques.SlideInRight)
                .duration(500)
                .playOn(textViewUserName);

        buttonRoutReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayoutStart.setVisibility(View.VISIBLE);
                linearLayoutPrice.setVisibility(View.GONE);
                linearLayoutEnd.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Trodini operation was successful", Toast.LENGTH_SHORT).show();
            }
        });
        buttonRoutStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startLocationName = editTextStartRoute.getText().toString().trim();
                if (startLocationName.isEmpty()) {
                    editTextStartRoute.setError("Provide start location");
                    return;
                }
                editTextStartRoute.setText("");
                PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("START", "start").commit();
                YoYo.with(Techniques.FadeOutUp)
                        .delay(100)
                        .withListener(new Animator.AnimatorListener() {
                            @Override
                            public void onAnimationStart(Animator animation) {

                            }

                            @Override
                            public void onAnimationEnd(Animator animation) {
                                linearLayoutEnd.setVisibility(View.VISIBLE);
                                linearLayoutStart.setVisibility(View.GONE);
                                YoYo.with(Techniques.FadeInDown)
                                        .duration(500)
                                        .playOn(linearLayoutEnd);

                            }

                            @Override
                            public void onAnimationCancel(Animator animation) {

                            }

                            @Override
                            public void onAnimationRepeat(Animator animation) {

                            }
                        })
                        .playOn(linearLayoutStart);

                buttonRoutEnd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        endLocationName = editTextEnd.getText().toString().trim();
                        if (endLocationName.isEmpty()) {
                            editTextStartRoute.setError("Provide an end location");
                            return;
                        }
                        editTextEnd.setText("");
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("START", "end").commit();
                        YoYo.with(Techniques.FadeOutUp)
                                .delay(100)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        linearLayoutPrice.setVisibility(View.VISIBLE);
                                        linearLayoutEnd.setVisibility(View.GONE);
                                        YoYo.with(Techniques.FadeInDown)
                                                .duration(500)
                                                .playOn(linearLayoutPrice);

                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(linearLayoutEnd);
                    }
                });

                buttonRoutPrice.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        price = editTextPrice.getText().toString().trim();
                        if (price.isEmpty()) {
                            editTextPrice.setError("Provide an price");
                            return;
                        }
                        editTextPrice.setText("");
                        PreferenceManager.getDefaultSharedPreferences(getActivity()).edit().putString("START", "price").commit();
                        YoYo.with(Techniques.FadeOutUp)
                                .delay(100)
                                .withListener(new Animator.AnimatorListener() {
                                    @Override
                                    public void onAnimationStart(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationEnd(Animator animation) {
                                        linearLayoutStart.setVisibility(View.VISIBLE);
                                        linearLayoutPrice.setVisibility(View.GONE);
                                        YoYo.with(Techniques.FadeInDown)
                                                .duration(500)
                                                .playOn(linearLayoutStart);
                                        Misc.showNotice(getActivity(), "Wohoooo, you have created a handle successfully");

                                    }

                                    @Override
                                    public void onAnimationCancel(Animator animation) {

                                    }

                                    @Override
                                    public void onAnimationRepeat(Animator animation) {

                                    }
                                })
                                .playOn(linearLayoutPrice);
                    }
                });


            }
        });


        return view;
    }


}