package com.trodini.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.trodini.app.R;
import com.trodini.app.adapter.HandleAdapter;
import com.trodini.app.adapter.HandleItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentHandle extends Fragment {
    private Intent intent;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_handle, container, false);

        intent = new Intent();

        final List<HandleItem> handleItemList = new ArrayList<HandleItem>();
        listView = (ListView) view.findViewById(R.id.listViewHandle);
        final HandleAdapter handleAdapter = new HandleAdapter(getActivity(), R.layout.layout_handle_item, handleItemList);
        listView.setAdapter(handleAdapter);
        handleItemList.add(new HandleItem("1", "20 mins", "Gh S 1.50", "American House", "Lapaz"));
        handleItemList.add(new HandleItem("2", "1hr 50 mins", "Gh S 12.50", "Madina", "Sogakope"));
        handleItemList.add(new HandleItem("3", "20 mins", "Gh S 1.50", "North Legon", "Lapaz"));
        handleItemList.add(new HandleItem("4", "20 mins", "Gh S 1.50", "American House", "Lapaz"));
        handleAdapter.notifyDataSetChanged();


        return view;
    }


}