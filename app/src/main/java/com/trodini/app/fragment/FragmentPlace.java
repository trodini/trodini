package com.trodini.app.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.melnykov.fab.ObservableScrollView;
import com.trodini.app.R;
import com.trodini.app.core.SuggestedRoute;
import com.trodini.app.persistence.User;
import com.trodini.app.util.Misc;

import java.util.List;


public class FragmentPlace extends Fragment {
    private ObservableScrollView scrollView;
    private Intent intent;
    private EditText editTextStartRoute, editTextEnd;
    private String start, end;
    private Button button;
    private Context context;
    private TextView textView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_place, container, false);


        button = (Button) view.findViewById(R.id.button);
        textView = (TextView) view.findViewById(R.id.textViewGreeting);
        editTextEnd = (EditText) view.findViewById(R.id.editTextEnd);
        editTextStartRoute = (EditText) view.findViewById(R.id.editTextStartRoute);


        intent = new Intent();
        context = getActivity();

        List<User> userList = User.listAll(User.class);
        User user;
        String first_name = "Trodini";
        if (!userList.isEmpty()) {
            user = userList.get(0);
            first_name = user.first_name;

        }

        textView.setText(Misc.getGreeting().concat(", ").concat(first_name));
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                start = editTextStartRoute.getText().toString().trim();
                end = editTextEnd.getText().toString().trim();
                if (start.equalsIgnoreCase("")) {
                    editTextStartRoute.setError("Required field");
                    return;
                }
                if (end.equalsIgnoreCase("")) {
                    editTextEnd.setError("Required field");
                    return;
                }
                new MaterialDialog.Builder(context)
                        .title("Trodini Notice")
                        .content("Finding the magic route for " + "\n" + start + " to " + end)
                        .negativeText("Cancel")
                        .positiveText("Ok")
                        .callback(new MaterialDialog.ButtonCallback() {
                            @Override
                            public void onPositive(MaterialDialog dialog) {
                                //make api request
                                intent.setClass(context, SuggestedRoute.class);
                                startActivity(intent);

                            }

                            @Override
                            public void onNegative(MaterialDialog dialog) {
                                dialog.dismiss();
                            }

                        })
                        .show();

            }
        });


        return view;
    }


}