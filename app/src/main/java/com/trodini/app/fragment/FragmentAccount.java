package com.trodini.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.trodini.app.R;
import com.trodini.app.core.ImagePreview;
import com.trodini.app.persistence.User;

import java.util.List;


public class FragmentAccount extends Fragment {
    private ImageView profileImage;
    private String defaultImage = "https://www.facebook.com/photo.php?fbid=10202979539919248&set=a.1434008259995.2057831.1526604372&type=1&theater";
    private Intent intent;
    private TextView textViewUserName;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_account, container, false);

        intent = new Intent();

        profileImage = (ImageView) view.findViewById(R.id.imageViewProfilePic);
        textViewUserName = (TextView) view.findViewById(R.id.textViewUserName);
        TextView textViewUserTrodes = (TextView) view.findViewById(R.id.textViewUserTrodes);
        textViewUserTrodes.setText("0 Trode Points");
        List<User> userList = User.listAll(User.class);
        User user;
        String first_name = "Trodini";
        String last_name = "Trodini";
        if (!userList.isEmpty()) {
            user = userList.get(0);
            first_name = user.first_name;
            last_name = user.last_name;
            defaultImage = user.image_link;
        }
        textViewUserName.setText(first_name + " " + last_name);
        Ion.with(profileImage)
                .error(R.color.primary_color)
                .placeholder(R.color.primary_color)
                .load(defaultImage);

        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.setClass(getActivity(), ImagePreview.class);
                startActivity(intent);

            }
        });

        return view;
    }


}