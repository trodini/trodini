package com.trodini.app.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.melnykov.fab.ObservableScrollView;
import com.trodini.app.R;
import com.trodini.app.adapter.FlagAdapter;
import com.trodini.app.adapter.FlagItem;
import com.trodini.app.adapter.HandleAdapter;
import com.trodini.app.adapter.HandleItem;

import java.util.ArrayList;
import java.util.List;


public class FragmentFlag extends Fragment {
    private ObservableScrollView scrollView;
    private Intent intent;
    private ListView listView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_flag, container, false);

        final List<FlagItem> flagItems = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.listViewFlag);
        final FlagAdapter flagAdapter = new FlagAdapter(getActivity(), R.layout.layout_flag_item, flagItems);
        listView.setAdapter(flagAdapter);
        flagItems.add(new FlagItem("1", "Traffic Flag"));
        flagItems.add(new FlagItem("2", "Road Diversion"));
        flagItems.add(new FlagItem("3", "Bad Road"));
        flagAdapter.notifyDataSetChanged();
        intent = new Intent();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });

        return view;
    }


}