package com.trodini.app.util;

/**
 * Created by bright on 2/7/15.
 */
public class ApiUrl {
    public static String baseUrl = "http://trodini.herokuapp.com/api/v1/";
    public static String user = "users/";
    public static String login = "login/";
    public static String signUp = "signup/";
    public static String travels = "travels/search/";
    public static String travel = "travels/";


    public static String getBaseUrl() {
        return baseUrl;
    }

    public static String getSignUp() {
        return baseUrl.concat(user).concat(signUp);
    }

    public static String getTravels() {
        return baseUrl.concat(travels);
    }
}
