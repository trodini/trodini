package com.trodini.app.util;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.afollestad.materialdialogs.MaterialDialog;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by bright on 2/6/15.
 */
public class Misc {


    public static String getGreeting() {

        Date date = new Date();
        SimpleDateFormat simpDate;

        simpDate = new SimpleDateFormat("kk:mm:ss");

        String hour = simpDate.format(date).split(":")[0];
        String greeting;

        if (Integer.parseInt(hour) < 12) {
            greeting = "Good Morning";
        } else if (Integer.parseInt(hour) > 12 && Integer.parseInt(hour) < 16) {
            greeting = "Good Afternoon";

        } else {
            greeting = "Good Evening";
        }
        return greeting;

    }

    public static void showNotice(final Context context, String message) {
        new MaterialDialog.Builder(context)
                .title("Trodini Notice")
                .content(message)
                .positiveText("Ok")
                .callback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        dialog.dismiss();
                    }

                })
                .show();
    }

    public static void sendFeedback(Context context) {
        Intent Email = new Intent(Intent.ACTION_SEND);
        Email.setType("text/email");
        Email.putExtra(Intent.EXTRA_EMAIL, new String[]{"trodini@gmail.com"});
        Email.putExtra(Intent.EXTRA_SUBJECT, "Feedback From Trodini App");
        Email.putExtra(Intent.EXTRA_TEXT, "Dear ...," + "");
        context.startActivity(Intent.createChooser(Email, "Trodini Feedback:"));

    }

}
